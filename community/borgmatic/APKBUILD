# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=borgmatic
pkgver=1.7.1
pkgrel=0
pkgdesc="Simple, configuration-driven backup software for servers and workstations"
url="https://torsion.org/borgmatic/"
license="GPL-3.0-or-later"
arch="noarch !s390x" # tests fail on s390x
depends="
	borgbackup
	python3
	py3-setuptools
	py3-jsonschema
	py3-requests
	py3-ruamel.yaml
	py3-colorama
	"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-flexmock
	"
source="$pkgname-$pkgver.tar.gz::https://projects.torsion.org/borgmatic-collective/borgmatic/archive/$pkgver.tar.gz
	python3.patch
	"
builddir="$srcdir/borgmatic"

build() {
	python3 setup.py build
}

check() {
	# omit a simple test that requires borgmatic to be available in $PATH
	pytest -k "not test_borgmatic_version_matches_news_version"
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir" --skip-build
}

sha512sums="
63cc2bced4851288ead08414a22858552f915380d84bb477a791642d2e2c7b99fc0f0982479c00391d31d232c2ef3a28d8b32870b63a834bb293fdf34a449c0f  borgmatic-1.7.1.tar.gz
c4561ec75f486f75121f18039e42b166ed7eac4f3a6a2e821410ec0b8f0d1a48f1e5155393d8b735b6554efcf9cfc8ff163bc64b262b0239f86a03ec59ab6652  python3.patch
"
