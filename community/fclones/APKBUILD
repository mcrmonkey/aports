# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=fclones
pkgver=0.27.2
pkgrel=0
pkgdesc="Efficient duplicate file finder"
url="https://github.com/pkolaczk/fclones"
license="MIT"
arch="all !s390x !riscv64" # blocked by rust/cargo
arch="$arch !armhf !armv7 !x86" # tests fail
makedepends="cargo eudev-dev"
source="https://github.com/pkolaczk/fclones/archive/v$pkgver/fclones-$pkgver.tar.gz
	musl-fix.patch
	"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/fclones "$pkgdir"/usr/bin/fclones
}

sha512sums="
fd3b7eb0cfcfb0dacf907a52711da86901c6111bb50bb98ec4695fcdfc87fdf431f2a48fb336b5a709a1af8c1bfd15951d9d7aa36ac0ed24269064c5a92931ac  fclones-0.27.2.tar.gz
bca0d44f67de1f16e2cbdccfe7fb5b0aea4cadf5912b548eaa584d3c40e32e1b8d81385a19dc48e2ef66143ac392c8538489c10c0f0fc60fb8f779446edc4d3a  musl-fix.patch
"
