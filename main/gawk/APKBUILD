# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Bartłomiej Piotrowski <nospam@bpiotrowski.pl>
# Contributor: TBK <alpine@jjtc.eu>
pkgname=gawk
pkgver=5.2.0
pkgrel=0
pkgdesc="GNU awk pattern-matching language"
url="https://www.gnu.org/software/gawk/gawk.html"
arch="all"
license="GPL-3.0-or-later"
checkdepends="coreutils diffutils"
subpackages="$pkgname-doc"
source="https://ftp.gnu.org/gnu/gawk/gawk-$pkgver.tar.xz
	echild-strerror.patch"

case "$CARCH" in
# TODO: sigpipe1 test fails
# See: https://lists.gnu.org/archive/html/bug-gawk/2021-02/msg00005.html
s390x) options="!check" ;; # sigpipe1 test fails
esac

prepare() {
	default_prepare

	# TODO: https://lists.gnu.org/archive/html/bug-gawk/2021-02/msg00005.html
	sed -i test/Makefile.in -e "/clos1way6/d"
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-nls
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
e81e1efb1be06f82602e704d10e8de4b78797d058d9718d353e0837660dc8adf952965240c0a3b1a71c3e295f2e9641eacf64496d1d896edd81b101e09a656ac  gawk-5.2.0.tar.xz
5bbb175da2d93c9c1d422a4e5a2c2400486c0204929ac6771bf2c2effbee37b84ef9441821a47e1fdc4a337e3cb8ad92cb67d473822876ea790dd373822d4dbd  echild-strerror.patch
"
