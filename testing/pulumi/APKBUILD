# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Contributor: Fraser Waters <frassle@gmail.com>
# Maintainer: Fraser Waters <frassle@gmail.com>
pkgname=pulumi
pkgver=3.39.1
pkgrel=0
pkgdesc="Infrastructure as Code SDK"
url="https://pulumi.com/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	$pkgname-language-go:_go
	$pkgname-language-nodejs:_nodejs
	$pkgname-language-python:_python
	$pkgname-language-dotnet:_dotnet
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi/archive/v$pkgver.tar.gz"
# Tests require runtimes for each language provider (dotnet6-sdk, python3, go, nodejs),
# also easily OOM
options="!check"

export CGO_ENABLED=0
export GOCACHE="$srcdir/go-cache"
export GOTMPDIR="$srcdir"
export GOMODCACHE="$srcdir/go"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	local _goldflags="-X github.com/pulumi/pulumi/pkg/v${pkgver%%.*}/version.Version=v$pkgver"
	mkdir -p "$builddir"/bin

	cd "$builddir"/pkg
	go build -v \
		-o "$builddir"/bin/pulumi \
		-ldflags "$_goldflags" \
		./cmd/pulumi

	cd "$builddir"/sdk
	go build -v \
		-o "$builddir"/bin/pulumi-language-go \
		-ldflags "$_goldflags" \
		./go/pulumi-language-go
	for lang in nodejs python dotnet; do
		go build -v \
			-o "$builddir"/bin/pulumi-language-$lang \
			-ldflags "$_goldflags" \
			./$lang/cmd/pulumi-language-$lang
	done

	cd "$builddir"
	./bin/pulumi gen-completion bash > pulumi.bash
	./bin/pulumi gen-completion fish > pulumi.fish
	./bin/pulumi gen-completion zsh > pulumi.zsh
}

package() {
	install -Dm755 bin/pulumi -t "$pkgdir"/usr/bin/

	install -Dm644 pulumi.bash \
		"$pkgdir"/usr/share/bash-completion/completions/pulumi
	install -Dm644 pulumi.fish \
		"$pkgdir"/usr/share/fish/completions/pulumi.fish
	install -Dm644 pulumi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_pulumi
}

_go() {
	pkgdesc="$pkgdesc (Go language provider)"
	depends="$pkgname=$pkgver-r$pkgrel"

	install -Dm755 "$builddir"/bin/pulumi-language-go -t "$subpkgdir"/usr/bin/
}

_nodejs() {
	pkgdesc="$pkgdesc (NodeJS language provider)"
	depends="$pkgname=$pkgver-r$pkgrel"

	install -Dm755 "$builddir"/bin/pulumi-language-nodejs -t "$subpkgdir"/usr/bin/
}

_python() {
	pkgdesc="$pkgdesc (Python language provider)"
	depends="$pkgname=$pkgver-r$pkgrel python3"

	install -Dm755 "$builddir"/bin/pulumi-language-python -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/cmd/pulumi-language-python-exec -t "$subpkgdir"/usr/bin/
}

_dotnet() {
	pkgdesc="$pkgdesc (.NET language provider)"
	depends="$pkgname=$pkgver-r$pkgrel"

	install -Dm755 "$builddir"/bin/pulumi-language-dotnet -t "$subpkgdir"/usr/bin/
}

sha512sums="
1d62772a83162e6e9869f0af72dd10e58c34950e0676b538cae8bfc167044c92db89710cd45a4ff33b1651c68a4b19a070c96b56bb833aed5e5edbd694bdeec4  pulumi-3.39.1.tar.gz
"
