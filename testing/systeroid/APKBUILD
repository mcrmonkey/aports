# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=systeroid
pkgver=0.2.2
pkgrel=0
pkgdesc="A more powerful alternative to sysctl(8)"
arch="all"
url="https://systeroid.cli.rs/"
license="Apache-2.0"
makedepends="cargo libxcb-dev"
checkdepends="linux-lts-doc xclip"
subpackages="$pkgname-doc $pkgname-tui:_tui $pkgname-tui-doc:_tui_doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/systeroid/archive/refs/tags/v$pkgver.tar.gz"

# tests get stuck on armhf,
# CONFIG_BSD_PROCESS_ACCT is not set for lts.ppc64le.config
case $CARCH in
	armhf|ppc64le) options="!check" ;;
esac

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --release --frozen -p systeroid
	cargo build --release --frozen --no-default-features -p systeroid-tui
}

check() {
	cargo test --release --frozen -p systeroid
	cargo test --release --frozen --no-default-features -p systeroid-tui
}

package() {
	install -Dm0755 target/release/systeroid target/release/systeroid-tui \
		-t "$pkgdir"/usr/bin/
	install -Dm0644 man8/systeroid.8 -t "$pkgdir"/usr/share/man/man8/
}

_tui() {
	pkgdesc="$pkgname terminal user interface"
	amove usr/bin/systeroid-tui
}

_tui_doc() {
	pkgdesc="$pkgname terminal user interface (documentation)"

	cd "$builddir"
	install -Dm0644 man8/systeroid-tui.8 -t "$subpkgdir"/usr/share/man/man8/

	default_doc
}

sha512sums="
4b472d55391e08142c742dd3b59c46f98b1625358760b56888fb8cf0e90aa2dc478565f1d71589cba74bb5cd1b7c705a8fc4ab673d9248227845bf17e381300a  systeroid-0.2.2.tar.gz
"
